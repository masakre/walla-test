# Welcome to Luis Gonzalez Wallapop test!

Hi! This is a front-end dev demo made for Wallapop.
This Angular 9 webapp is a little item manager, where you can:

 - Search items by title, description, email or price
 - Save (and remove) items in a wishlist.
 
## Demo

[Open link 😉](https://walla-test-1c8b9.firebaseapp.com/home)

## Requirements
- NodeJS 10/12
[Download from official website](https://nodejs.org/en/download/)

- Angular 9/10
> npm install -g @angular/cli

- (*Optional)* Node Version Manager (nvm), useful tool to manage NodeJS versions.
[Download from GitHub](https://github.com/nvm-sh/nvm#installing-and-updating)

## How to run the app
Clone this repo, use your terminal to move into the cloned repo folder an run:
> npm install
 
to install dependencies.

To run the app:
> ng serve -o

## How to  test

Tests are made with [Jasmine](https://jasmine.github.io/) and run then with [Karma](https://karma-runner.github.io/latest/index.html)

To run them:
> ng test

and open http://localhost:9876/ in your browser

## To improve 😓
Due the lack of time some features couldn't be implemented, here is a list of them:

- Theme the app using Bootstrap variables.
- In mobile device, use Intersection Observer API to get new items as you scroll and show a floating Wishlist icon-only button a bottom right.
- Add mor.e performance tweaks


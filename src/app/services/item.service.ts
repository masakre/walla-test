import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ItemModel } from '../models/item';
import { ItemFilteroptions } from '../models/ItemFilterOptions';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  readonly ENDPOINT = 'https://frontend-tech-test-data.s3.eu-west-1.amazonaws.com/items.json';
  readonly ITEM_OFFSET = 5;

  constructor(private http: HttpClient) { }

  getItems(opts?: ItemFilteroptions): Promise<ItemModel[]> {

    const promise = new Promise<ItemModel[]>((resolve, reject) => {
      this.http.get(this.ENDPOINT)
        .toPromise()
        .then((response: any) => {
          let { items } = response;
          const start = opts?.pagination?.start || 0;
          const end = start + opts?.pagination?.end || this.ITEM_OFFSET;
          // seach filter
          items = items.filter((item) => this.itemMatchesOpts(item, opts));
          // pagination
          items = items.slice(start, end);
          // parse price to number
          items = items.map((item) => {
            return { ...item, price: parseFloat(item.price) };
          });

          resolve(items);

        })
        .catch(err => reject(err));
    });

    return promise;
  }


  itemMatchesOpts(item: any, opts: ItemFilteroptions): boolean {
    if (!opts || !opts.searchBy) { return true; }

    if (opts.searchBy
      && opts.searchBy.value) {

      try {
        const key = opts.searchBy.key || 'title';
        // price must be equal
        if (key === 'price') {
          return (`${item.price}` === opts.searchBy.value);
          // find a fragment in desired key.
        } else {
          if (item[key].toLowerCase().indexOf(opts.searchBy.value.toLowerCase()) !== -1) { return true; }
        }

      } catch (error) {
        throw new Error(error);
      }

    }

    return false;
  }


}



import { TestBed } from '@angular/core/testing';

import { WishlistService } from './wishlist.service';
import { ItemModel } from '../models/item';

describe('WishlistService', () => {
  let service: WishlistService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WishlistService);
    service.engine = fakeLocalStorage;
    service.WISHLIST_KEY = 'wishlist_test';
    service.engine.clear();
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('add item on empty wishlist and retrieve it', () => {

    service.addItem(testItem);
    const wishlist = service.getItems();
    expect(wishlist[0].title).toEqual((testItem.title));
  });


  it('add multiple items on non-empty wishlist', () => {

    service.addItem(testItem);
    service.addItem(testItem2);

    const wishlist = service.getItems();

    expect(wishlist[0].title).toEqual((testItem.title));
    expect(wishlist[1].title).toEqual((testItem2.title));
    expect(wishlist[1].image).toEqual((testItem2.image));

  });


  it('add the same item on wishlist should return false', () => {
    service.addItem(testItem);
    expect(service.addItem(testItem)).toBeFalse();

    const wishlist = service.getItems();

    expect(wishlist.length).toEqual(1);
  });


  it('remove item', () => {
    service.addItem(testItem);
    service.addItem(testItem2);

    expect(service.removeItem(testItem)).toBeTruthy();

    const wishlist = service.getWishlist();
    expect(wishlist.length).toEqual(1);
    expect(wishlist[0].title).toEqual(testItem2.title);

  });


  it('setWishlist emits the new wishlist when wishlist is empty', () => {
    service.clear();
    service.addItem(testItem);

    const wishList$ = service.wishListAsObservable;
    wishList$.subscribe(wishlist => {
      expect(wishlist[0].title).toBe(testItem.title);
    });
  });


  it('setWishlist emits the new wishlist when wishlist is NOT empty', () => {
    service.clear();
    service.addItem(testItem);
    service.addItem(testItem2);

    const wishList$ = service.wishListAsObservable;
    wishList$.subscribe(wishlist => {
      expect(wishlist.length).toBe(2);
      expect(wishlist[0].title).toBe(testItem.title);
      expect(wishlist[1].title).toBe(testItem2.title);
    });
  });


  it('setWishlist emits the new wishlist when wishlist is NOT empty (remove test)', () => {
    service.clear();
    service.addItem(testItem);
    service.addItem(testItem2);
    service.removeItem(testItem2);

    const wishList$ = service.wishListAsObservable;
    wishList$.subscribe(wishlist => {
      expect(wishlist.length).toBe(1);
      expect(wishlist[0].title).toBe(testItem.title);
    });
  });

});


let store = {};

const fakeLocalStorage = {
  getItem: (key) => {
    return store[key] || null;
  },
  setItem: (key, value) => {
    store[key] = value + '';
  },
  removeItem: (key) => {
    delete store[key];
  },
  clear: () => {
    store = {};
  },
  key: (index) => {
    return null;
  },
  length: null,
};


const testItem = new ItemModel();
testItem.title = 'item title';
testItem.description = 'item description';
testItem.email = 'test@email.com';
testItem.price = 10;
testItem.image = 'https://frontend-tech-test-data.s3-eu-west-1.amazonaws.com/img/iphone.png';

const testItem2 = new ItemModel();
testItem2.title = 'test item 2';
testItem2.description = 'test item 2 description';
testItem2.email = 'test@email.com';
testItem2.image = 'https://www.fancyimage.com/img2.png';
testItem2.price = 3000;

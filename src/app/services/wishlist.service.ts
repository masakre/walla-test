import { Injectable } from '@angular/core';
import { ItemFilteroptions } from '../models/ItemFilterOptions';
import { ItemModel } from '../models/item';
import { Observable, of, BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class WishlistService {

  engine = sessionStorage;
  WISHLIST_KEY = 'wishlist';
  wishList$ = new BehaviorSubject([]);

  constructor() {
    this.engine.clear();
  }

  get wishListAsObservable(): Observable<ItemModel[]> {
    return this.wishList$.asObservable();
  }


  getItems(filter?: ItemFilteroptions): ItemModel[] {
    try {
      const wishlist = this.getWishlist();
      if (!wishlist) { return []; }
      if (!filter) { return wishlist; }

    } catch (error) {
      throw new Error(error);
    }
  }


  removeItem(item: ItemModel) {
    try {

      let wishlist = this.getWishlist();
      if (!wishlist) { return; }
      wishlist = wishlist.filter((i: ItemModel) => i.title !== item.title);
      this.setWishlist(wishlist);
      return wishlist;

    } catch (error) {
      throw new Error(error);
    }
  }


  addItem(item: ItemModel): boolean {
    try {
      let wishlist = this.getWishlist();
      // no repeated items in wishlist allowed
      if (wishlist.findIndex((itm: ItemModel) => itm.title === item.title) !== -1) {
        return false;
      }

      if (!wishlist || !wishlist.length) { wishlist = [item]; }
      else { wishlist.push(item); }

      this.setWishlist(wishlist);
      return true;

    } catch (error) {
      throw new Error(error);
    }
  }

  clear() {
    this.engine.clear();
  }

  getWishlist() {
    try {
      return JSON.parse(this.engine.getItem(this.WISHLIST_KEY)) || [];

    } catch (error) {
      throw new Error(error);
    }
  }

  setWishlist(newWishlist: ItemModel[]) {
    try {

      this.engine.setItem(this.WISHLIST_KEY, JSON.stringify(newWishlist));
      this.wishList$.next(newWishlist);

    } catch (error) {
      throw new Error(error);
    }
  }
}

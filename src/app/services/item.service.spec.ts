import { TestBed } from '@angular/core/testing';

import { ItemService } from './item.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ItemModel } from '../models/item';

describe('ItemService', () => {
  let service: ItemService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ItemService]
    });
    // service = TestBed.inject(ItemService);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ItemService);
  });


  afterEach(() => {
    httpTestingController.verify();
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('(getItems) should return a list of items and first item must match', () => {
    service.getItems()
      .then(items => {
        expect(items).toBeInstanceOf(Array);
        expect(items).toBeInstanceOf(Array);
        expect(items.length).toBe(testItems.length);
        expect(items[0].title).toBe('item1');
        expect(items[0].price).toBe(100);
      });

    const req = httpTestingController.expectOne(service.ENDPOINT);

    expect(req.request.method).toEqual('GET');

    req.flush(testItems);
  });

});


const testItem = new ItemModel();
testItem.title = 'item title';
testItem.description = 'item description';
testItem.email = 'test@email.com';
testItem.price = 10;
testItem.image = 'https://frontend-tech-test-data.s3-eu-west-1.amazonaws.com/img/iphone.png';

const testItems: ItemModel[] = [
  { title: 'item1', description: 'item1 description', email: 'test@email.com', image: 'http://wwww.fancyimage.com/img.png', price: 100 },
  { title: 'item2', description: 'item2 description', email: 'test@email.com', image: 'http://wwww.fancyimage.com/img.png', price: 10 },
  { title: 'item3', description: 'item3 description', email: 'test@email.com', image: 'http://wwww.fancyimage.com/img.png', price: 300 },
  { title: 'item4', description: 'item4 description', email: 'test@email.com', image: 'http://wwww.fancyimage.com/img.png', price: 50 }
];

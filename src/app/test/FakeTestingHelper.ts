import { ItemModel } from '../models/item';
import { ItemFilteroptions } from '../models/ItemFilterOptions';
import { of } from 'rxjs/internal/observable/of';
import { BehaviorSubject, Observable } from 'rxjs';

export class FakeWishlistService {
  fakeStore: ItemModel[] = [];
  wishList$ = new BehaviorSubject(this.fakeStore);

  get wishListAsObservable(): Observable<ItemModel[]> {
    return this.wishList$.asObservable();
  }

  constructor(store: ItemModel[] = [testItem, testItem2]) {
    this.fakeStore = store;
  }

  getItems() {
    return this.fakeStore;
  }

  removeItem(item: ItemModel) {
    this.fakeStore = this.fakeStore.filter(itm => itm.title !== item.title);
    return this.fakeStore;
  }

  addItem(item: ItemModel) {
    if (this.fakeStore.findIndex(itm => itm.title === item.title) !== -1) {
      return false;
    }
    this.fakeStore.push(item);
  }
}


export class FakeItemService {
  getItems(opts?: ItemFilteroptions) {
    return Promise.resolve([testItem, testItem2]);
  }
}

export const testItem = new ItemModel();
testItem.title = 'item title';
testItem.description = 'item description';
testItem.email = 'test@email.com';
testItem.price = 10;
testItem.image = 'https://frontend-tech-test-data.s3-eu-west-1.amazonaws.com/img/iphone.png';

export const testItem2 = new ItemModel();
testItem2.title = 'test item 2';
testItem2.description = 'test item 2 description';
testItem2.email = 'test@email.com';
testItem2.image = 'https://www.fancyimage.com/img2.png';
testItem2.price = 3000;

export const testItemGenerator = (count: number): ItemModel[] => {
  if (!count) { return []; }
  const items = [];

  for (let i = 0; i < count; i++) {
    const item = new ItemModel();
    item.description = `${i} item description`;
    item.title = `${i} item title `;
    item.email = `${i} item email`;
    item.price = (i + 1 * 10);
    item.image = `https://fakeimage.com/img${i}.png`;
    items.push(item);
  }

  return items;
};


export const fakeEvent = (value: string) => {
  return {
    target: { value }
  };
};

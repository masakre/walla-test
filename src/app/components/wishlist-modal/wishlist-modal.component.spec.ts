import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WishlistModalComponent } from './wishlist-modal.component';
import { WishlistService } from 'src/app/services/wishlist.service';
import { By } from '@angular/platform-browser';
import { FakeWishlistService, testItem, testItem2, fakeEvent } from 'src/app/test/FakeTestingHelper';

describe('WishlistModalComponent', () => {
  let component: WishlistModalComponent;
  let fixture: ComponentFixture<WishlistModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WishlistModalComponent],
      providers: [
        { provide: WishlistService, useClass: FakeWishlistService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WishlistModalComponent);
    component = fixture.componentInstance;
    component.show = true;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


  describe('alert-warning test', () => {
    it('should show a alert-warning if wishlist is empty', () => {
      component.items = component.itemsToShow = [];

      fixture.detectChanges();

      expect(component.items).toEqual([]);

      const alertElem = fixture.debugElement.query(By.css('.alert-warning'));
      expect(alertElem).toBeTruthy();
    });


    it('should NOT show a alert-warning if there are items', () => {
      component.items = component.itemsToShow = [testItem];
      fixture.detectChanges();

      expect(component.items).toEqual([testItem]);
      expect(component.itemsToShow).toEqual([testItem]);

      const alertElem = fixture.debugElement.query(By.css('.alert-warning'));
      expect(alertElem).toBeFalsy();
      // const itemTitleElem = fixture.debugElement.query(By.css('.item .card-title'));
      // expect(itemTitleElem.nativeElement.textContent.trim()).toBe(testItem.title.trim());
    });


    it('alert-warning shown after remove all items', () => {
      component.items = component.itemsToShow = [testItem, testItem2];

      fixture.detectChanges();

      component.onItemBtnClick(testItem);
      component.onItemBtnClick(testItem2);

      fixture.detectChanges();
      console.log(component);
      expect(component.items.length).toBe(0);
      expect(component.itemsToShow.length).toBe(0);

      const alertElem = fixture.debugElement.query(By.css('.alert-warning'));
      expect(alertElem).toBeTruthy();
    });
  });


  it('should show app-item-list if there are items in wishlist ', () => {
    component.items = component.itemsToShow = [testItem, testItem2];
    fixture.detectChanges();

    const itemElems = fixture.debugElement.queryAll(By.css('app-items-list'));
    expect(itemElems.length).toBeTruthy();
  });


  it('should remove item', () => {
    spyOn(component.wishlistService, 'getItems').and.returnValue([testItem, testItem2]);
    component.ngOnInit();

    fixture.detectChanges();

    component.onItemBtnClick(testItem);

    fixture.detectChanges();

    expect(component.items.length).toBe(1);
    expect(component.itemsToShow.length).toBe(1);
  });




  // SEARCH IN PUT TEST
  describe('search input tests', () => {

    it('searchInputChange return false if there are no items', () => {
      spyOn(component.wishlistService, 'getItems').and.returnValue([]);

      expect(component.onSearchInputChange(null)).toBeFalsy();
    });


    it('searchInputChange return falsy if there search key length is <= 2', () => {
      spyOn(component.wishlistService, 'getItems').and.returnValue([testItem, testItem2]);

      expect(component.onSearchInputChange(fakeEvent('a'))).toBeFalsy();
      expect(component.onSearchInputChange(fakeEvent('ab'))).toBeFalsy();
    });


    it('searchInputChange filters if search key matches', () => {
      component.items = component.itemsToShow = [testItem, testItem2];
      fixture.detectChanges();

      component.onSearchInputChange(fakeEvent('test'));
      fixture.detectChanges();

      expect(component.items.length).toBe(2);
      expect(component.itemsToShow.length).toBe(1);

      const itemsListElem = fixture.debugElement.queryAll(By.css('app-items-list'));
      expect(itemsListElem.length).toBeTruthy();
    });
  });


  it('searchInputChange do not filter if search key do not matches', () => {
    component.items = component.itemsToShow = [testItem, testItem2];

    component.onSearchInputChange(fakeEvent('no match'));
    fixture.detectChanges();

    expect(component.items.length).toBe(2);
    expect(component.itemsToShow.length).toBe(0);

  });


});


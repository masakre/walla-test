import { Component, OnInit, Output, EventEmitter, Input, OnDestroy, SimpleChange, SimpleChanges, OnChanges } from '@angular/core';
import { WishlistService } from '../../services/wishlist.service';
import { ItemModel } from 'src/app/models/item';

@Component({
  selector: 'app-wishlist-modal',
  templateUrl: './wishlist-modal.component.html',
  styleUrls: ['./wishlist-modal.component.scss']
})
export class WishlistModalComponent implements OnInit, OnDestroy, OnChanges {

  @Output() closeModal = new EventEmitter<boolean>();
  @Input() show: boolean;

  items: ItemModel[] = [];
  itemsToShow: ItemModel[] = [];
  searchStr = '';

  constructor(public wishlistService: WishlistService) { }


  ngOnInit(): void {
    // workarround to lock body scroll
    document.getElementsByTagName('body')[0].classList.remove('modal-open');
  }


  ngOnDestroy(): void {
    // workarround to unlock body scroll
    document.getElementsByTagName('body')[0].classList.remove('modal-open');
  }


  ngOnChanges(changes: SimpleChanges) {
    const body = document.getElementsByTagName('body')[0];
    const { show } = changes;
    if (show.currentValue === true) {

      body.classList.add('modal-open');
      this.items = this.wishlistService.getItems();
      this.itemsToShow = this.items;

    }
    else {
      body.classList.remove('modal-open');
      this.itemsToShow = this.items = [];
    }
  }


  onItemBtnClick(item: ItemModel) {

    const items = this.wishlistService.removeItem(item);
    if (!items) { return; }

    this.itemsToShow = this.items = items;
  }


  onSearchInputChange(e) {
    if (!this.items.length) { return; }

    const str = e.target.value.toLowerCase();
    // if no str to filter, return all items
    if (str.length === 0) { this.itemsToShow = this.items; }
    // str must be larger than 2 to start filtering
    if (str.length <= 2) { return; }

    this.itemsToShow = this.items
      .filter((item: ItemModel) => item.title.toLowerCase().indexOf(str) !== -1);

  }


}

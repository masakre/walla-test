import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { fakeEvent } from '../../test/FakeTestingHelper';
import { SearchFormComponent } from './search-form.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('SearchFormComponent', () => {
  let component: SearchFormComponent;
  let fixture: ComponentFixture<SearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchFormComponent],
      imports: [RouterTestingModule.withRoutes([
        { path: 'search/:id', component: MockComponent }
      ])]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('searchStr <= 2 is invalid', () => {
    component.searchStr = '';
    expect(component.isSearchFormValid()).toBeFalse();

    component.searchStr = 'a';
    expect(component.isSearchFormValid()).toBeFalse();

    component.searchStr = 'ab';
    expect(component.isSearchFormValid()).toBeFalse();
  });


  it('searchStr > 2 is valid', () => {
    component.searchStr = 'abc';
    expect(component.isSearchFormValid()).toBeTrue();

    component.searchStr = 'abcdef';
    expect(component.isSearchFormValid()).toBeTrue();
  });


  it('navigates when searchStr length is bigger than 2', () => {
    const router = TestBed.inject(Router);
    spyOn(router, 'navigateByUrl');
    component.searchStr = 'iphone';
    component.search();
    expect(router.navigateByUrl).toHaveBeenCalled();
  });


  it('does not route searchStr length is <= 2', () => {
    const router = TestBed.inject(Router);
    component.searchStr = 'ei';
    expect(component.search()).toBeFalsy();

    component.searchStr = 'a';
    expect(component.search()).toBeFalsy();
  });

  it('should return false when email is invalid', () => {
    component.searchBy = 'email';

    component.searchStr = 'a';
    expect(component.isSearchFormValid()).toBeFalse();

    component.searchStr = 'a@';
    expect(component.isSearchFormValid()).toBeFalse();

    component.searchStr = 'a@wallapop';
    expect(component.isSearchFormValid()).toBeFalse();

    component.searchStr = 'a@wallapop.';
    expect(component.isSearchFormValid()).toBeFalse();
  });

  it('should return true when email is valid', () => {
    component.searchBy = 'email';

    component.searchStr = 'a@wallapop.com';
    expect(component.isSearchFormValid()).toBeTrue();
  });


  it('when searchBy equals price, search input type must be "number and viceversa"', () => {
    component.handleSearchByChange(fakeEvent('price'));
    fixture.detectChanges();

    const searchInput = fixture.debugElement.query(By.css('[name="searchStr"]'));
    expect(searchInput.nativeElement.getAttribute('type')).toBe('number');
    expect(searchInput.nativeElement.getAttribute('min')).toBe('0');

    component.handleSearchByChange(fakeEvent('title'));
    fixture.detectChanges();

    expect(searchInput.nativeElement.getAttribute('type')).toBe('text');
    expect(searchInput.nativeElement.getAttribute('min')).toBeFalsy();
  });



});


@Component({
})
class MockComponent {

}


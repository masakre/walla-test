import { Component, OnInit, Input, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @Input() searchStr = '';
  @Input() searchBy = 'title';
  @ViewChild('searchStrInput') searchStrInput: ElementRef;

  constructor(private route: Router, private renderer: Renderer2) { }

  ngOnInit(): void {
    if (!this.searchBy) { this.searchBy = 'title'; }
  }


  search() {
    if (!this.isSearchFormValid()) { return; }
    this.route.navigateByUrl(`/home/${this.searchStr}/${this.searchBy}`);
  }


  isSearchFormValid() {
    switch (this.searchBy) {
      case 'price':
        return this.searchStr.length > 0;
      case 'email':
        return (this.searchStr.length > 5 && this.validateEmail(this.searchStr));
      default:
        return (!!this.searchStr && this.searchStr.length > 2);
    }
  }


  handleSearchByChange(ev) {
    this.searchStr = '';
    this.searchBy = ev.target.value;
    if (ev.target.value === 'price') {
      this.renderer.setAttribute(this.searchStrInput.nativeElement, 'type', 'number');
      this.renderer.setAttribute(this.searchStrInput.nativeElement, 'min', '0');
      this.renderer.setAttribute(this.searchStrInput.nativeElement, 'placeholder', 'Type price');
    } else {
      this.renderer.setAttribute(this.searchStrInput.nativeElement, 'type', 'text');
      this.renderer.removeAttribute(this.searchStrInput.nativeElement, 'min');
      this.renderer.setAttribute(this.searchStrInput.nativeElement, 'placeholder', (ev.target.value === 'email') ?
        'Type valid email' : 'Search awesome items');


    }
  }

  reset() {
    this.route.navigateByUrl('/home');
  }

  validateEmail(mail: string) {
    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail));
  }
}

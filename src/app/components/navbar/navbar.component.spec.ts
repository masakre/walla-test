import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarComponent } from './navbar.component';
import { By } from '@angular/platform-browser';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavbarComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should render nav-brand, nav-brand-img', () => {
    const navBrandElem = fixture.debugElement.query(By.css('.navbar-brand'));
    const navBrandImgElem = fixture.debugElement.query(By.css('.navbar-brand img'));

    expect(navBrandElem.nativeElement.textContent.trim()).toBe('Luis Gonzalez');
    expect(navBrandImgElem.nativeElement.src.indexOf('wala-logo.svg')).toBeGreaterThan(-1);
  });


});


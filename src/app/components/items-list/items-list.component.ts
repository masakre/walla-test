import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ItemModel } from 'src/app/models/item';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss']
})
export class ItemsListComponent implements OnInit {

  @Input() items: ItemModel[];
  @Input() wishedItems: ItemModel[];
  @Input() sortBy = '';
  @Input() btnClasses = 'btn-primary';
  @Input() btnText = `<i class="fa fa-heart"></i>`;
  @Input() displayMode = 'large';
  @Input() btnDisabled = false;
  @Output() btnClick = new EventEmitter<ItemModel>();

  constructor() { }

  ngOnInit(): void {
  }


  handleSortByChange(ev) {
    const key = ev.target.value;
    if (!key || key === '' || !this.items || !this.items.length) { return; }

    this.items = this.items.sort((a, b) => {
      const elemA = (key === 'price') ? parseFloat(a[key]) : a[key].toLowerCase();
      const elemB = (key === 'price') ? parseFloat(b[key]) : b[key].toLowerCase();

      if (elemA < elemB) { return -1; }
      if (elemA === elemB) { return 0; }
      if (elemA > elemB) { return 1; }
    });
  }

  isWished(item: ItemModel): boolean {
    if (!this.wishedItems || !this.wishedItems.length) { return false; }
    return (this.wishedItems.findIndex(itm => itm.title === item.title) !== -1);
  }

  onBtnClick(item: ItemModel) {
    console.log('onBtnClick', item);
    this.btnClick.emit(item);
  }


}

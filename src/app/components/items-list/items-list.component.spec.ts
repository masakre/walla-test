import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { fakeEvent, testItem, testItem2, testItemGenerator } from 'src/app/test/FakeTestingHelper';

import { ItemsListComponent } from './items-list.component';

describe('ItemsListComponent', () => {
  let component: ItemsListComponent;
  let fixture: ComponentFixture<ItemsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ItemsListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should show 5 items when there are 5 items', () => {
    const ITEMS_COUNT = 5;
    component.items = testItemGenerator(ITEMS_COUNT);
    fixture.detectChanges();

    const appItemElems = fixture.debugElement.queryAll(By.css('app-item'));
    expect(appItemElems.length).toBe(ITEMS_COUNT);
  });

  it('should show filter and app-items if there are items', () => {
    component.items = [testItem, testItem2];
    fixture.detectChanges();

    const itemFilterSelect = fixture.debugElement.query(By.css('select#item-filter'));
    expect(itemFilterSelect.nativeElement).toBeTruthy();
    expect(itemFilterSelect.nativeElement.nodeName).toBe('SELECT');
    expect(itemFilterSelect.nativeElement[0].value).toBe('');
    expect(itemFilterSelect.nativeElement[1].value).toBe('title');
    expect(itemFilterSelect.nativeElement[2].value).toBe('description');
    expect(itemFilterSelect.nativeElement[3].value).toBe('price');
    expect(itemFilterSelect.nativeElement[4].value).toBe('email');

    const appItemElems = fixture.debugElement.queryAll(By.css('app-item'));
    expect(appItemElems.length).toBe(2);
  });


  it('should NOT show filter and app-items if there are NO items', () => {
    component.items = [];
    fixture.detectChanges();

    const itemFilterSelect = fixture.debugElement.query(By.css('select#item-filter'));
    expect(itemFilterSelect).toBeFalsy();

    const appItemElems = fixture.debugElement.queryAll(By.css('app-item'));
    expect(appItemElems.length).toBe(0);
  });


  it('should sort by title, description, price', () => {
    component.items = testItemGenerator(5);
    component.handleSortByChange(fakeEvent('title'));
    fixture.detectChanges();

    let index1: number = parseFloat(component.items[0].title.slice(0, 1));
    let index2: number = parseFloat(component.items[1].title.slice(0, 1));
    let index3: number = parseFloat(component.items[2].title.slice(0, 1));

    expect(index1).toBeLessThan(index2);
    expect(index2).toBeLessThan(index3);


    component.handleSortByChange(fakeEvent('description'));
    fixture.detectChanges();

    index1 = parseFloat(component.items[0].description.slice(0, 1));
    index2 = parseFloat(component.items[1].description.slice(0, 1));
    index3 = parseFloat(component.items[2].description.slice(0, 1));

    expect(index1).toBeLessThan(index2);
    expect(index2).toBeLessThan(index3);

    component.handleSortByChange(fakeEvent('description'));
    fixture.detectChanges();

    index1 = parseFloat(component.items[0].description.slice(0, 1));
    index2 = parseFloat(component.items[1].description.slice(0, 1));
    index3 = parseFloat(component.items[2].description.slice(0, 1));

    expect(index1).toBeLessThan(index2);
    expect(index2).toBeLessThan(index3);

  });


  it('isWished should return false when item is not wished and viceversa', () => {
    component.items = [testItem, testItem2];
    component.wishedItems = [];
    fixture.detectChanges();
    expect(component.isWished(testItem)).toBeFalse();

    component.wishedItems = [testItem];
    fixture.detectChanges();
    expect(component.isWished(testItem)).toBeTrue();
  });


});

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ItemModel } from 'src/app/models/item';
import { DomSanitizer } from '@angular/platform-browser';
import { WishlistService } from 'src/app/services/wishlist.service';


@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent {

  @Input() item: ItemModel;
  // @Input() index: number;
  @Input() disableAddToWishlist = false;
  @Input() btnClasses = 'btn-primary';
  @Input() btnText = `<i class="fa fa-heart"></i>`;
  @Input() displayMode = 'large';
  @Input() btnDisabled = false;
  @Output() buttonClick = new EventEmitter<ItemModel>();


  constructor(private wishlistService: WishlistService) { }


}

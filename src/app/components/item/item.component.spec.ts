import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemComponent } from './item.component';
import { By } from '@angular/platform-browser';
import { SafePipeModule } from 'safe-pipe';
import { ItemModel } from 'src/app/models/item';


describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SafePipeModule
      ],
      declarations: [ItemComponent],
      providers: [

      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    component.item = testItem;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should render', () => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;

    component.item = testItem;
    fixture.detectChanges();
    const itemElem = fixture.debugElement.query(By.css('.item'));
    expect(itemElem.nativeElement).toBeInstanceOf(HTMLElement);
  });

  it('should NOT render', () => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;

    component.item = null;
    fixture.detectChanges();
    const itemElem = fixture.debugElement.query(By.css('.item'));
    expect(itemElem).toBe(null);
  });


  it('should render image, title, description and button', () => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;

    component.item = testItem;
    fixture.detectChanges();

    const imgElem = fixture.debugElement.query(By.css('img'));
    const titleElem = fixture.debugElement.query(By.css('.card-title'));
    const descriptionElem = fixture.debugElement.query(By.css('.card-text'));
    const btnElem = fixture.debugElement.query(By.css('button'));

    expect(imgElem.nativeElement).toBeTruthy();
    expect(titleElem.nativeElement.textContent).toBe(testItem.title);
    expect(descriptionElem.nativeElement.textContent).toBe(testItem.description);
    expect(btnElem.nativeElement).toBeTruthy();
  });


  it('button should use btnClasses & btnText', () => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    const btnClasses = 'btn-danger';
    const btnText = 'Remove';

    component.item = testItem;
    component.btnClasses = btnClasses;
    component.btnText = btnText;
    fixture.detectChanges();

    const btnElem = fixture.debugElement.query(By.css('button'));

    expect(btnElem.nativeElement.classList.contains(btnClasses)).toBeTrue();
    expect(btnElem.nativeElement.textContent).toBe(btnText);
  });


  it('button should emit the item', () => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    const btnClasses = 'btn-danger';
    const btnText = 'Remove';

    component.item = testItem;
    component.btnClasses = btnClasses;
    component.btnText = btnText;
    fixture.detectChanges();

    const btnElem: HTMLButtonElement = fixture.debugElement.query(By.css('button')).nativeElement;

    spyOn(component.buttonClick, 'emit');
    btnElem.click();

    fixture.detectChanges();
    expect(component.buttonClick.emit).toHaveBeenCalledWith(testItem);
  });





});


const testItem = new ItemModel();
testItem.title = 'item title';
testItem.description = 'item description';
testItem.email = 'test@email.com';
testItem.price = 10;
testItem.image = 'https://frontend-tech-test-data.s3-eu-west-1.amazonaws.com/img/iphone.png';

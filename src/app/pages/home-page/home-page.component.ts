import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { ItemService } from '../../services/item.service';
import { ItemModel } from '../../models/item';

import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { ItemFilteroptions } from '../../models/ItemFilterOptions';
import { WishlistService } from 'src/app/services/wishlist.service';
import { Subscription } from 'rxjs';
// import swal from 'sweetalert';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {

  items: ItemModel[] = null;
  showLoading = false;
  searchStr = '';

  showMoreButton = true;
  showWishlist: boolean;
  itemsFilter: ItemFilteroptions;
  paramsSubs: Subscription;
  wishListSubs: Subscription;
  wishList: ItemModel[] = [];
  errorMessage: string;
  scrollListener;

  constructor(public itemService: ItemService,
    public activatedRoute: ActivatedRoute,
    public wishlistService: WishlistService,
    private route: Router,
    private renderer2: Renderer2) {

    this.itemsFilter = {
      pagination: { start: 0, end: this.itemService.ITEM_OFFSET },
      searchBy: null
    };

    // this.scrollListener = this.renderer2.listen(window, 'scroll', (e) => {
    //   console.log('[scroll listener]', e);
    // });
  }


  ngOnInit(): void {
    try {

      this.wishListSubs = this.wishlistService.wishListAsObservable
        .subscribe(this.handlNewWishList.bind(this));

      this.paramsSubs = this.activatedRoute.paramMap
        .subscribe(this.handleRouteParamsChange.bind(this));

    } catch (error) {
      this.handleError(error);
    }
  }


  ngOnDestroy() {
    this.paramsSubs.unsubscribe();
    this.wishListSubs.unsubscribe();
    // this.scrollListener();
  }


  addToWishlist(item: ItemModel) {
    console.log('adding to wishlist...', item);
    try {

      if (!this.wishlistService.addItem(item)) { alert('Item already in wishlist'); }

    } catch (error) {
      this.handleError(error);
    }
  }


  handleRouteParamsChange(paramMap: ParamMap) {
    // search must be performed
    if (paramMap.has('searchStr')) {
      this.items = [];
      this.itemsFilter.pagination.start = 0;
      this.showMoreButton = true;
      // set the key to filter by title
      this.itemsFilter.searchBy = {
        key: paramMap.get('searchBy'),
        value: paramMap.get('searchStr')
      };

      // show items that match the filter
      this.showLoading = true;
      this.itemService.getItems(this.itemsFilter)
        .then(this.handleNewItems.bind(this))
        .catch(this.handleError.bind(this));

      // no search string, therefore get all items
    } else {

      this.itemService.getItems()
        .then(this.handleNewItems.bind(this))
        .catch(this.handleError.bind(this));
    }
  }


  handleError(error) {
    this.errorMessage = (typeof error === 'string') ?
      error : (error.message && typeof error.message === 'string') ?
        error.message : 'Something went wrong...';
    console.log('%c[ERROR] !!!', 'font-size:14px; font-weight:bold; color:red', { error });
  }


  downloadMoreItems() {
    this.showLoading = true;
    this.itemService.getItems(this.itemsFilter)
      .then(this.handleNewItems.bind(this))
      .catch(this.handleError.bind(this));

  }


  handleNewItems(items: ItemModel[]) {
    this.items = (!this.items || !this.items.length) ?
      items : this.items.concat(items);
    this.showLoading = false;
    this.itemsFilter.pagination.start = this.items.length;
    // if no more items left...
    if (items.length < this.itemService.ITEM_OFFSET) { this.showMoreButton = false; }
  }


  handlNewWishList(newWishList: ItemModel[]) {
    this.wishList = newWishList;
    console.log('newWishList', { newWishList, _this: this });
  }


}

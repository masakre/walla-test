import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HomePageComponent } from './home-page.component';
import { ItemService } from 'src/app/services/item.service';
import { WishlistService } from 'src/app/services/wishlist.service';
import { FakeWishlistService, FakeItemService, testItem, testItem2, testItemGenerator, fakeEvent } from 'src/app/test/FakeTestingHelper';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Location } from '@angular/common';


describe('HomePageComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HomePageComponent],
      providers: [
        { provide: ItemService, useClass: FakeItemService },
        { provide: WishlistService, useClass: FakeWishlistService },
      ],
      imports: [RouterTestingModule.withRoutes([
        { path: 'home', component: HomePageComponent },
        { path: 'search/:key', component: HomePageComponent },
        { path: '', pathMatch: 'full', redirectTo: 'home' },
        { path: '**', pathMatch: 'full', redirectTo: 'home' }
      ])]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('initial route path should be /home', fakeAsync(() => {
    const location = TestBed.inject(Location);
    const router = TestBed.inject(Router);
    router.initialNavigation();
    router.navigate(['/']);
    tick();

    expect(location.path()).toBe('/home');
  }));


  it('search/:key should render HomePage', fakeAsync(() => {
    const location = TestBed.inject(Location);
    const router = TestBed.inject(Router);
    router.initialNavigation();
    router.navigate(['search', 'test']);
    tick();
    expect(location.path()).toBe('/search/test');

    component.ngOnInit();
    tick();

  }));


  it('should create two subscriptions on init', () => {
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.paramsSubs).toBeTruthy();
  });


  it('should show NO ITEMS FOUND when no items found', () => {
    component.items = [];
    component.showLoading = false;
    component.itemsFilter.searchBy = { value: 'asdf', key: 'title' };
    fixture.detectChanges();

    const alertElem = fixture.debugElement.query(By.css('.alert-danger'));
    expect(alertElem.nativeElement.textContent.trim()).toBe('NO ITEMS FOUND');
  });



  it('should show items-list, wishlist button and Download More if there are items', () => {
    component.items = [testItem, testItem2];
    fixture.detectChanges();

    const itemsListComp = fixture.debugElement.query(By.css('app-items-list'));
    expect(itemsListComp.nativeElement).toBeTruthy();

    const showWLBtn = fixture.debugElement.query(By.css('#show-wishlist-btn'));
    expect(showWLBtn.nativeElement.textContent.toLowerCase().trim()).toBe('wishlist');

    const showMoreBtn = fixture.debugElement.query(By.css('#show-more-btn'));
    expect(showMoreBtn.nativeElement.textContent.toLowerCase().trim()).toBe('show more items');
  });


  it('should NOT show items-list, wishlist button if there are NO items', () => {
    component.items = [];
    fixture.detectChanges();

    const itemsListElem = fixture.debugElement.query(By.css('app-items-list'));
    expect(itemsListElem).toBeFalsy();

    const showWLBtn = fixture.debugElement.query(By.css('#show-wishlist-btn'));
    expect(showWLBtn).toBeFalsy();
  });


  it('show more btn should call itemsService.getItems', () => {
    const ITEMS_COUNT = 5;
    component.items = testItemGenerator(ITEMS_COUNT);
    component.showLoading = false;
    component.showMoreButton = true;
    fixture.detectChanges();

    spyOn(component.itemService, 'getItems');
    const showMoreBtnElem = fixture.debugElement.query(By.css('#show-more-btn'));
    showMoreBtnElem.nativeElement.click();
    fixture.detectChanges();

    expect(component.itemService.getItems).toHaveBeenCalled();
  });


  it('show more btn should be hidden when showMoreBtn is false', () => {

    component.items = [testItem, testItem2];
    component.showMoreButton = false;
    fixture.detectChanges();

    const showMoreBtnElem = fixture.debugElement.query(By.css('#show-more-btn'));
    expect(showMoreBtnElem).toBeFalsy();
  });



  it('should call whislistService.addItem & cannot add the same item to wishlist', () => {
    component.items = [testItem, testItem2];
    component.showLoading = false;
    fixture.detectChanges();

    spyOn(component.wishlistService, 'addItem');
    spyOn(window, 'alert');

    component.addToWishlist(component.items[0]);

    expect(component.wishlistService.addItem).toHaveBeenCalled();

    component.addToWishlist(component.items[0]);

    expect(window.alert).toHaveBeenCalled();

  });


  it('should show wishlist when btn clicked', () => {
    component.items = [testItem, testItem2];
    component.showLoading = false;
    fixture.detectChanges();

    const showWLBtnElem = fixture.debugElement.query(By.css('#show-wishlist-btn'));
    showWLBtnElem.nativeElement.click();

    fixture.detectChanges();
    expect(component.showWishlist).toBeTrue();

    const wlModalElem = fixture.debugElement.query(By.css('app-wishlist-modal'));
    expect(wlModalElem).toBeTruthy();
  });


  it('should show badge when whistlist is NOT empty', () => {
    component.items = testItemGenerator(5);
    component.wishList = [testItem, testItem2];
    component.showLoading = false;
    component.errorMessage = null;
    fixture.detectChanges();

    const badgeElem = fixture.debugElement.query(By.css('#show-wishlist-btn .badge'));
    expect(badgeElem).toBeTruthy();
    expect(badgeElem.nativeElement.textContent.trim()).toBe(component.wishList.length.toString());
  });


  it('should NOT show badge when whistlist is empty', () => {
    component.items = testItemGenerator(5);
    component.wishList = null;
    component.showLoading = false;
    component.errorMessage = null;
    fixture.detectChanges();

    const badgeElem = fixture.debugElement.query(By.css('#show-wishlist-btn .badge'));
    expect(badgeElem).toBeFalsy();

    component.wishList = [];
    fixture.detectChanges();

    expect(badgeElem).toBeFalsy();

  });











});


export class ItemFilteroptions {
  searchBy?: {
    key: string,
    value: string
  };
  pagination?: {
    start?: number;
    end?: number;
  };
}

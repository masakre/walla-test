export class ItemModel {
  title: string;
  description: string;
  image: string;
  price: number;
  email: string;
}
